$(document).ready(function(){


    var socket = io();

    $("#logout").click(function(){
        $("#page2").hide();
        $("#page1").show();
    });

    $("#loginForm").submit(function(e){
        var loginData = {
            username: $("#inputEmail").val(),
            password: $("#inputPassword").val(),
            role: $("#loginSelect").val()
        };
        socket.emit("login", loginData);
        socket.on('login-invalid', function(){
            alert("login-invalid");
        });
        socket.on('login-valid', function(){
            
            socket.emit('load');
            $("#page1").hide();
            $("#page2").show();
        });

        return false;
    });

    $("#signUpForm").submit(function(e){
        var loginData = {
            username: $("#signUpinputEmail").val(),
            password: $("#signUpinputPassword").val(),
            role: $("#signUpSelect").val()
        };
        socket.emit("signUp", loginData);
        socket.on('signUp-invalid', function(){
            alert("signUp-invalid");
        });
        socket.on('signUp-valid', function(){
            
            socket.emit('load');
            $("#page1").hide();
            $("#page2").show();
        });

        return false;
    });

    $("#addExpense").submit(function (e) {

        var args = {
                date: $("#inputDate").val(),
                amount : $("#inputExpense").val(),
                desc : $("#inputDesc").val()
         };
        socket.emit("add",args);
        return false;
    });


    $("#editExpense").submit(function (e) {

        var args = {
                date: $("#inputEditDate").val(),
                amount : $("#inputEditExpense").val(),
                desc : $("#inputEditDesc").val(),
                srNo : $("#srNoHidden").val()                
         };
        //alert(args.date+" "+args.amount+" "+args.desc);
        socket.emit("update",args);
        return false;
    });

    $("#report").submit(function (e) {

        var args = {
                startdate: $("#startDate").val(),
                enddate : $("#endDate").val()
         };
        socket.emit("report",args);
        $("#history").hide();
        $("#reports").show();
        return false;
    });

    socket.on('element_deleted',function(){
        alert("One entry deleted");

    });

    socket.on('element_added', function (data) {
        $("#historyTableBody").html("");
        var i=0;
        while(data[i] != null)
        {
            $("#historyTableBody").append("<tr class='historyTableRow clickable-row' id=''><td id='srNo' style='display:none'>"+data[i].srNo+"</td><td id='date'>"+data[i].dateTime+"</td><td id='amount'>"+data[i].amount+"</td><td id='desc'>"+data[i].description+"</td><td><button type='button' id = 'delete' class='btn' style='margin-left:2px'>X</button></td></tr>");
            i++;
        }

        $('.historyTableRow').click(function (e) {
            var date = $(this).find('#date').text();
            var amount = $(this).find('#amount').text();
            var desc = $(this).find('#desc').text();
            var srNo = $(this).find('#srNo').text();
            $("#inputEditDate").val(date);
            $("#inputEditExpense").val(amount);
            $("#inputEditDesc").val(desc);
            $("#srNoHidden").val(srNo);
        });

        $('.historyTableRow button').click(function (e) {
            var srNo = $(this).parent().parent().find('#srNo').text();
            
            socket.emit('delete-entry', srNo);
        });

    });
    
    
});