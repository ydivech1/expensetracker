var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var userNameIn;
var passwordIn;
var masterData;
var useridIn;

app.use(express.static("public"));

app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');

});

http.listen(process.env.PORT || 3000, function(){
    console.log('listening on *:3000');
});

io.on('connection', function(socket) {
    console.log('a user connected');

    socket.on('disconnect', function(){


        console.log('user disconnected');
    });
});

var mysql = require("mysql");

// First you need to create a connection to the db
var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "yash123",
  database: "expense"
});

con.connect(function(err){
  if(err){
    console.log('Error connecting to Db');
    return;
  }

  console.log('Connection established');
});


io.on('connection', function (socket) {
    socket.on('add', function (data) {
                
         con.query('INSERT INTO expensedetails (userName,dateTime,amount, description) VALUES ("'+userNameIn+'","'+ data.date +'","'+data.amount+'","'+data.desc+'")', function(err,rows){
          if(err) throw err;    
          console.log('Data added to Db:\n');
          masterData = rows;
          console.log(masterData);
        });
         con.query('select srNo,dateTime,amount, description from expensedetails where userName="'+userNameIn+'"', function(err,rows){
          if(err) throw err;
          console.log('Data retrived from Db:\n');
          masterData = rows;
          console.log(masterData);
          socket.emit('element_added', masterData);
        });

        
    });

    socket.on('update', function (data) {

          console.log(data.date+" "+data.amount+" "+data.desc+ " "+data.srNo);       
          con.query('UPDATE expensedetails SET dateTime="'+ data.date +'", amount="'+ data.amount +'", description="'+ data.desc +'" WHERE srNo = "'+ data.srNo +'"',function(err,rows){
          if(err) throw err;
          console.log('Data updated to Db:\n');
          masterData = rows;
          console.log(masterData);
        });       
         con.query('select srNo,dateTime,amount, description from expensedetails where userName="'+userNameIn+'"', function(err,rows){
          if(err) throw err;
          console.log('Data retrived from Db:\n');
          masterData = rows;
          console.log(masterData);
          socket.emit('element_added', masterData);
        });

        
    });

    socket.on('report', function (data) {
         console.log
         con.query('SELECT * from expensedetails where userName= "'+userNameIn+'" AND dateTime >= "'+data.startdate+'" AND dateTime <= "'+data.enddate+'"', function(err,rows){
          if(err) throw err;
          console.log('Data added to Db:\n');
          masterData = rows;    
          console.log("Report Generated.");
          console.log(masterData);
          socket.emit('element_added', masterData);
        });                           
        
    });



    socket.on('load',function(){
        con.query('select srNo,dateTime,amount, description from expensedetails where userName="'+userNameIn+'"', function(err,rows){
          if(err) throw err;
          console.log('Data retrived from Db:\n');
          masterData = rows;
          console.log(masterData);
          socket.emit('element_added', masterData);
        });
    });

    socket.on('delete-entry',function(data){
        console.log("srno: "+data.srNo);
        con.query('delete from expensedetails where srNo="'+data+'"', function(err,rows){
          if(err) throw err;
          console.log('Entry deleted.:\n');
          socket.emit('element_deleted');

               con.query('select srNo,dateTime,amount, description from expensedetails where userName="'+userNameIn+'"', function(err,rows){
              if(err) throw err;
              console.log('Data retrived from Db:\n');
              masterData = rows;
              console.log(masterData);
              socket.emit('element_added', masterData);
            });
        });
       
    });

    socket.on('login',function(data){
        userNameIn = data.username;
        passwordIn = data.password;
        con.query('select * from users where userName="'+userNameIn+'" and password="'+passwordIn+'"', function(err,rows){
          if(err)
            {
                socket.emit('login-invalid');
            }
            else
            {
                masterData = rows;
                
                 if(masterData[0]==undefined)
                  {
                    socket.emit('login-invalid');
                  }
                  else{
                    socket.emit('login-valid');
                    console.log(masterData[0].userName);
                  }

              
            }
        });
       
    });

socket.on('signUp',function(data){
        userNameIn = data.username;
        passwordIn = data.password;
        var userRoleIn = data.role;
        con.query('INSERT INTO users (userName,password,userRole) VALUES ("'+userNameIn+'","'+passwordIn+'","'+userRoleIn+'")', function(err,rows){
          if(err)
            {
                socket.emit('signUp-invalid');
            }
            else
            {
                masterData = rows;
                socket.emit('signUp-valid');
            }
        });
       
    });

});


